<?php

require_once(dirname(__FILE__) ."/classes/animal.php");
require_once(dirname(__FILE__) ."/classes/ape.php");
require_once(dirname(__FILE__) ."/classes/frog.php");


$sheep = new Animal("sheep");
echo "Name: ".$sheep->getName()."<br>";
echo "Legs: ".$sheep->getLegs()."<br>";
echo "Cold blooded: ".$sheep->getColdblooded()."<br>"."<br>";

$kodok = new Frog("buduk");
echo "Name: ".$kodok->getName()."<br>";
echo "Legs: ".$kodok->getLegs()."<br>";
echo "Cold Blooded: ".$kodok->getColdblooded()."<br>";
echo "Jump: ".$kodok->jump()."<br>"."<br>";

$sungokong = new Ape("kera sakti");
echo "Name: ".$sungokong->getName()."<br>";
echo "Legs: ".$sungokong->getLegs()."<br>";
echo "Cold Blooded: ".$sungokong->getColdblooded()."<br>";
echo "Yell: ".$sungokong->yell()."<br>"."<br>";
?>